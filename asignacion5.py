from flask import Flask, render_template, url_for, request, redirect
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///tarea1.db'

class asig5(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	content = db.Column(db.String(200), nullable=False)
	date_created = db.Column(db.DateTime, default=datetime.utcnow)

@app.route ('/')
def tarea1():
    return 'palabras del diccionario'

@app.route ('asig1')
def asig1(asig1):
    return 'mouse dispositivo apuntador'

@app.route('/hola/<nombre>')
def saludar_nombre(nombre):
    return 'hola %s!' % nombre

if __name__ == '__main__':
    app.run()
